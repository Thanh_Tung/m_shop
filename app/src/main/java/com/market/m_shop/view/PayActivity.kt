package com.market.m_shop.view

import android.content.Intent
import android.os.Build
import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import com.market.m_shop.ItemDelayClickListener
import com.market.m_shop.R
import com.market.m_shop.model.BillModel
import kotlinx.android.synthetic.main.activity_pay.*

class PayActivity : AppCompatActivity() {

    var mSelect = ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_pay)

        btn_Pay.setOnClickListener(mSetOnClickListener)
        btn_continue.setOnClickListener(mSetOnClickListener)


    }

    private fun chooseTheFormOfDelivery(txtCheck: TextView, txtNoCheck: TextView) {
        txtCheck.setTextColor(getIdColor(R.color.color_Facebook))
        txtNoCheck.setTextColor(getIdColor(R.color.black))
    }

    private fun getIdColor(idcolor: Int): Int {
        var color = 0
        color = if (Build.VERSION.SDK_INT > 21) {
            ContextCompat.getColor(this, idcolor)
        } else {
            resources.getColor(idcolor)
        }
        return color
    }

    private val mSetOnClickListener = ItemDelayClickListener {
        when (it) {
            btn_Pay -> {
                var name = edt_name?.text.toString()
                var phone = edt_phone?.text.toString()
                var address = edt_address?.text.toString()
                if (name.trim().isNotEmpty() && phone.trim().isNotEmpty() && address.trim()
                        .isNotEmpty()
                ) {
                    if (cbThoaThuan.isChecked) {
                        BillModel()
                    }

                }
            }

            btn_continue -> {
                val iPay = Intent(this, HomeActivity::class.java)
                startActivity(iPay)
            }
            imgCOD -> {
                chooseTheFormOfDelivery(txtCOD, txtTransfer)
                mSelect = "COD"
            }
            img_transfer -> {
                chooseTheFormOfDelivery(txtTransfer, txtCOD)
                mSelect = "NOCOD"
            }

        }
    }
}
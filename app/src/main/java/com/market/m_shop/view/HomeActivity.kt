package com.market.m_shop.view

import android.content.Intent
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.ActionBarDrawerToggle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.ViewCompat
import com.example.m_shop.view.fragment.CoatFragment
import com.example.m_shop.view.fragment.JeanFragment
import com.example.m_shop.view.fragment.RoofFragment
import com.example.m_shop.view.fragment.SportFragment
import com.google.android.gms.auth.api.Auth
import com.google.android.material.appbar.AppBarLayout
import com.market.m_shop.R
import com.market.m_shop.adapter.ViewpagerAdapter
import com.market.m_shop.view.fragment.TshirtFragment
import kotlinx.android.synthetic.main.activity_home.*

class HomeActivity : AppCompatActivity() {
    var locButton: MenuItem? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_home)
        setupViewPager()
        initActionView()
        setSupportActionBar(toolbar)

        val actionBar = supportActionBar
        actionBar?.title = ""
        supportActionBar!!.setHomeButtonEnabled(true) //gán nút menu
        supportActionBar!!.setDisplayHomeAsUpEnabled(true)

        val drawerToggle: ActionBarDrawerToggle = object : ActionBarDrawerToggle(
            this,
            drawer,
            toolbar,
            R.string.open,
            R.string.close
        ) {

        }

        drawerToggle.isDrawerIndicatorEnabled = true
        drawer.addDrawerListener(drawerToggle)
        drawerToggle.syncState()

    }


    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        locButton = menu!!.findItem(R.id.itSearch)
        return true
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {

        val id = item.itemId
        when (id) {
            R.id.itDangNhap ->  {
                val iLogin = Intent(this, LoginActivity::class.java)
                startActivity(iLogin)
            }
        }
        return true

        return super.onOptionsItemSelected(item)
    }

    private fun initActionView() {
        app_bar_layout?.addOnOffsetChangedListener(AppBarLayout.OnOffsetChangedListener { appBarLayout, verticalOffset ->
            if (coollapToolbar.height + verticalOffset <= 1.5 * ViewCompat.getMinimumHeight(
                    coollapToolbar
                )
            ) {
                lnsearch.alpha = 0F
                locButton?.isVisible = true
            } else {
                lnsearch.alpha = 1F
                locButton?.isVisible = false
            }
        })
    }

    private fun setupViewPager() {
        val adapter = ViewpagerAdapter(supportFragmentManager)
        adapter.addFragment(TshirtFragment(), "Tshirt")
        adapter.addFragment(CoatFragment(), "Coat")
        adapter.addFragment(JeanFragment(), "Jeans")
        adapter.addFragment(SportFragment(), "Sportswear")
        adapter.addFragment(RoofFragment(), "Roof cover")
        viewpager.adapter = adapter
        tabs.setupWithViewPager(viewpager)
    }
}


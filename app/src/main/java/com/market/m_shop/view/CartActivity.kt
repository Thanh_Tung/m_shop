package com.market.m_shop.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import androidx.appcompat.app.AppCompatActivity
import com.market.m_shop.ItemDelayClickListener
import com.market.m_shop.R
import com.market.m_shop.model.ProductModel
import kotlinx.android.synthetic.main.activity_cart.*


class CartActivity : AppCompatActivity() {
    var productCode :  String?= ""
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cart)
        btnBuyNow?.setOnClickListener(mSetOnClickListener)
        getData()
    }

    private fun getData() {
        productCode = intent.extras?.get("productcode") as String?
        Log.e("tungnt", "getData: "+productCode )
    }

    private val mSetOnClickListener = ItemDelayClickListener {
        when (it) {
            btnBuyNow -> {
                val iPay = Intent(this, PayActivity::class.java)
                startActivity(iPay)
            }
        }
    }
}
package com.market.m_shop.view

import android.content.Intent
import android.os.Bundle
import android.util.Log
import android.view.Menu
import android.view.MenuItem
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.core.view.MenuItemCompat
import com.market.m_shop.ItemDelayClickListener
import com.market.m_shop.R
import com.market.m_shop.model.ProductModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.custom_layout_cart.*
import kotlinx.android.synthetic.main.fragment_detail_product.*
import java.util.*

@Suppress("UNREACHABLE_CODE")
class DetailProduct : AppCompatActivity() {
    var locButton: MenuItem? = null
    private var customCart: View? = null
    private var productList: List<ProductModel>? = ArrayList<ProductModel>()
    var product: ProductModel? = null
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.fragment_detail_product)
        getData()



        //khởi tạo tootlbar
        setSupportActionBar(toolbar)
        btnBuyNow?.setOnClickListener(mSetOnClickListener)
        imgCar?.setOnClickListener(mSetOnClickListener)
    }

    private fun getData() {
        product = intent.extras?.getSerializable("abc") as? ProductModel
        tvNameProduct?.text = product?.nameProduct
        txtGiaTien?.text = product?.price.toString() + "VNĐ"
        Picasso.with(this).load(product?.largePhoto).into(imgProductDetail)
    }

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        menuInflater.inflate(R.menu.menu_home, menu)
        locButton = menu!!.findItem(R.id.itGioHang)
        customCart = MenuItemCompat.getActionView(locButton)
        customCart?.setOnClickListener(mSetOnClickListener)
        return true
    }

    private val mSetOnClickListener = ItemDelayClickListener {
        when (it) {
            btnBuyNow -> {
                val iPay = Intent(this, PayActivity::class.java)
                startActivity(iPay)
            }
            //add giỏ hàng
            imgCar -> {
                product?.let { it1 -> (productList as ArrayList<ProductModel>).add(it1) }
                txtNumberAmount?.text = productList?.size.toString()
                Log.e("tungnt", "size: " + productList?.size)
            }
            // chuyển qua giỏ hàng
            customCart -> {
                val iPay = Intent(this, CartActivity::class.java)
                iPay.putExtra("productcode",product?.codeProduct)
                startActivity(iPay)
            }
        }
    }
}
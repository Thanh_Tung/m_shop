package com.example.m_shop.view.fragment

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.GridLayoutManager
import com.market.m_shop.CreateData
import com.market.m_shop.CreateData.Companion.createData
import com.market.m_shop.R
import com.market.m_shop.adapter.ProductAdapter
import com.market.m_shop.model.ProductModel
import com.market.m_shop.view.DetailProduct
import kotlinx.android.synthetic.main.fragment_tshirt.*
import java.util.*

class JeanFragment : Fragment(), ProductAdapter.SetOnItemSelectListener {
    private var mListProduct = ArrayList<ProductModel>()
    private var mListProductJean = ArrayList<ProductModel>()

    private val mAdapter by lazy {
        context?.let { ProductAdapter(it) }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        return inflater!!.inflate(R.layout.fragment_tshirt, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        mListProductJean.clear()
        mListProduct.clear()
        mListProduct= createData()
        for (item in mListProduct){
            if (item.productTypeCode == 1) {
                mListProductJean.add(item)
            }
        }


        showListProduct(mListProductJean)
        setRecyclerView()
        mAdapter?.setOnItemSelectProductListener(this)


    }

    private fun setRecyclerView() {
        rv_tshirt?.layoutManager =
            GridLayoutManager(context, 3, GridLayoutManager.VERTICAL, false)
        rv_tshirt?.setHasFixedSize(true)
        rv_tshirt?.adapter = mAdapter
    }

    private fun showListProduct(mListProductModel: ArrayList<ProductModel>) {
        mAdapter?.setData(mListProductModel)
    }


    override fun onItemSelectProductListener(product: ProductModel) {
        val intent: Intent = Intent(context, DetailProduct::class.java)
        intent.putExtra("abc", product)
        startActivity(intent)


    }
}
package com.market.m_shop.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.market.m_shop.R
import com.market.m_shop.model.ProductModel
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_product.view.*

class ProductAdapter(private val context: Context) :
    RecyclerView.Adapter<ProductAdapter.ProductViewholder>() {

    private val mListProduct: ArrayList<ProductModel> = ArrayList()
    private var listener: SetOnItemSelectListener? = null

    fun setData(list: List<ProductModel>) {
        mListProduct.clear()
        mListProduct.addAll(list)
        notifyDataSetChanged()
    }

    fun setOnItemSelectProductListener(listener: SetOnItemSelectListener) {
        this.listener = listener
    }

    inner class ProductViewholder(view: View) : RecyclerView.ViewHolder(view) {
        fun bind(product: ProductModel, listener: SetOnItemSelectListener?) {
            with(itemView) {
                tv_nameProduct?.text = product.nameProduct.toString()
                txtMoney?.text = product.amount.toString() + " VNĐ"
                Picasso.with(context).load(product.largePhoto).into(imgProduct)
            }

            itemView.setOnClickListener {
                listener?.onItemSelectProductListener(product)
            }
        }
    }

    interface SetOnItemSelectListener {
        fun onItemSelectProductListener(product: ProductModel)
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): ProductAdapter.ProductViewholder {
        val view = LayoutInflater.from(context).inflate(R.layout.item_product, parent, false)
        return ProductViewholder(
            view
        )
    }

    override fun onBindViewHolder(holder: ProductAdapter.ProductViewholder, position: Int) {
        holder.setIsRecyclable(false)
        val document = mListProduct[position]
        document.let { holder.bind(document, listener) }
    }

    override fun getItemCount(): Int {
        return mListProduct.size
    }
}
package com.market.m_shop

import android.os.Handler
import android.view.View

class ItemDelayClickListener(private var delayMillis : Long = Constants.CLICK_INTERVAL , private
val function: (view: View?) -> Unit) : View.OnClickListener {
    companion object {
        var enableToClick = true
    }

    override fun onClick(v: View?) {
        if(enableToClick) {
            if(delayMillis > 0){
                enableToClick = false
                Handler().postDelayed({ enableToClick = true }, delayMillis)
            }
            function.invoke(v)
        }
    }
}
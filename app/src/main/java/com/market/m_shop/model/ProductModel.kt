package com.market.m_shop.model

import java.io.Serializable

data class ProductModel(
//    var MASP: Int = 0,
//    var GIA: Int = 0,
//    var SOLUONG: Int = 0,
//    var MALOAISP: Int = 0,
//    var MATHUONGHIEU: Int = 0,
//    var LUOTMUA: Int = 0,
//    var MANV: Int = 0,
//    var SOLUONGTONKHO: Int = 0,
//    var ANHLON: String? = null,
//    var ANHNHO: String? = null,
//    var THONGTIN: String? = null,
//    var TENSP: String? = null,
//    var TENNV: String? = null,
//    var chiTietSanPhamList: List<ChiTietSanPham?>? = null,
//    var chiTietKhuyenMai: ChiTietKhuyenMai? = null

    var codeProduct: Int = 0,
    var price: Int = 0,
    var amount: Int = 0,
    var productTypeCode: Int = 0,
    var brandCode: Int = 0,
    var buyTimes: Int = 0,
    var employeeCode: Int = 0,
    var inventoryNumber: Int = 0,
    var largePhoto: String? = null,
    var smallPhoto: String? = null,
    var info: String? = null,
    var nameProduct: String? = null,
    var nameEmployee: String? = null,
//    var listDetailProduct: List<ChiTietSanPham?>? = null,
//    var detailPromotions: ChiTietKhuyenMai? = null
) : Serializable

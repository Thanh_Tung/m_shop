package com.market.m_shop.model

data class PromotionsModel(
    //    var MAKM = 0
//    var MALOAISP:Int = 0
//    var TENKM: String? = null
//    var NGAYBATDAU:String? = null
//    var NGAYKETTHUC:kotlin.String? = null
//    var HINHKHUYENMAI:kotlin.String? = null
//    var TENLOAISP:kotlin.String? = null
    val promotionCode: String,
    val productTypeCode: String,
    val namePromotions: String,
    val dateStart: String,
    val dateEnd: String,
    val imagePromotions: String,
    val nameProductType: String


)
package com.market.m_shop.model

data class BillModel(
//    var MaHD: Int = 0,
//    var NgayMua: String? = null,
//    var NgayGiao: String? = null,
//    var TrangThai: String? = null,
//    var TenNguoiNhan: String? = null,
//    var SoDT: String? = null,
//    var DiaChi: String? = null,
//    var MaChuyenKhoan: String? = null,
//    var ChuyenKhoan: String? = null

    var codeBill: Int = 0,
    var dateOfPurchase: String? = null,
    var deliveryDate: String? = null,
    var status: String? = null,
    var recipientName: String? = null,
    var phone: String? = null,
    var address: String? = null,
    var transferCode: String? = null,
    var transfer: String? = null
)
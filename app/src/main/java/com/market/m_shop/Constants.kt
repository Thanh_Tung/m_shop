package com.market.m_shop


object Constants {

    const val INT_NINE = 9
    const val SIX_MONTH = 6
    const val PATH_VIEW_TICKET = "ViewTicket"
    const val PATH_APPROVAL_TICKET_BROWSER = "ApprovalTicketBrowser"
    const val PARAM_ID = "id"
    const val PARAM_PHASE_ID = "phaseId"
    const val PARAM_TENANT_ID = "tenantId"
    const val KEY_SHOW_DIALOG_FTS_GROUP = "KEY_SHOW_DIALOG_FTS_GROUP"
    const val KEY_NAME_GROUP = "KEY_NAME_GROUP"
    const val KEY_GROUP_ID = "KEY_GROUP_ID"
    const val KEY_APP_PRODUCT = "KEY_APP_PRODUCT"
    const val KEY_PRODUCT_ID = "KEY_PRODUC_ID"
    const val CODE_RESULT_SUCCESS_1 = 1
    const val CODE_RESULT_SUCCESS_200 = 200
    const val DURATION_400 = 400
    const val YEAR_2020 = 2020
    const val YEAR_2021 = 2021
    const val YEAR_2022 = 2022
    const val YEAR_2023 = 2023
    const val YEAR_2024 = 2024
    const val DURATION_500 = 500L
    const val ALPHA_06 = 0.6F
    const val ALPHA_00 = 0.0F
    const val DURATION_1000 = 1000
    const val ONE_HOUR = 60 * 60 * 1000
    const val TIME_24_HOUR = 24 * 60 * 60 * 1000
    const val DECEMBER_CALENDAR = 11
    const val END_DATE_OF_DECEMBER = 31

    const val NAME_BROADCAST_NOTIFICATION_EBUS = BuildConfig.APPLICATION_ID + ".NOTIFICATION.EBUS.SYNC"

    const val LINK_APP_UTOP = "https://s.utop.vn/myfpt"

    const val POSITION_NONE = -1
    const val POSITION_FIRST = 0
    const val POSITION_SECOND = 1
    const val POSITION_THIRD = 2
    const val POSITION_FOURTH = 3
    const val POSITION_FIFTH = 4
    const val POSITION_SIXTH = 5
    const val POSITION_SEVENTH = 6
    const val POSITION_TENTH = 10
    const val FIRST_ITEM_COUNT = 1

    const val STATUS_CODE_GET_SERVER_SUCCESS = 200
    const val STATUS_CODE_GET_FROM_CACHE_VALID = 2020
    const val STATUS_CODE_GET_FROM_CACHE = -1
    const val STATUS_CODE_RESPONSE_FAIL = -2

    const val STATUS_CODE_CAN_NOT_CONNECT_TO_SERVER = 0

    const val STATUS_CODE_SHOW_OOPS_DIALOG__1 = -1
    const val STATUS_CODE_SHOW_OOPS_DIALOG_400 = 400

    const val STATUS_CODE_BOOK_ME_FAIL_133 = 133
    const val STATUS_CODE_BOOK_ME_FAIL_401 = 401

    const val STATUS_CODE_REDEEM_FAIL_112 = 112
    const val STATUS_CODE_REDEEM_FAIL_113 = 113

    const val STATUS_CODE_APPROVE_NOW_102 = 102
    const val STATUS_CODE_APPROVE_NOW_503 = 503

    const val STATUS_CODE_AUTHEN_MYFPT_401 = 401
    const val STATUS_CODE_AUTHEN_MYFPT_131 = 131

    const val STATUS_CODE_AUTHEN_MYFPT_440 = 440
    const val STATUS_CODE_AUTHEN_MYFPT_441 = 441
    const val STATUS_CODE_AUTHEN_ADFS_445 = 445
    const val STATUS_CODE_AUTHEN_ADFS_446 = 446

    const val NEGATIVE_ONE_LONG = -1L
    const val NEGATIVE_ONE_INT = -1
    const val ACTION_NAVIGATE_ETOP_500 = -100
    const val INT_ZERO = 0
    const val INT_ONE = 1
    const val INT_TWO = 2
    const val INT_THREE = 3
    const val INT_FOUR = 4
    const val INT_TEN = 10
    const val INT_FIVE = 5
    const val LONG_ZERO = 0L
    const val FLOAT_ZERO = 0F
    const val DOUBLE_ZERO = 0.0
    const val DOUBLE_ONE = 1.0
    const val LONG_ONE = 1L
    const val FLOAT_ONE = 1F
    const val STRING_ZERO = "0"
    const val STRING_TRUE = "true"
    const val DEFAULT_SIZE = 10
    const val BETA_SCREEN_PRE_LOAD = 3
    const val PAGE_SIZE_DEFAULT = 20
    const val PAGE_SIZE_HIGHLIGHT_DEFAULT = 5
    const val DEFAULT_VALUE = -1
    const val POSITION_WIDGET_NEWS = 998
    const val POSITION_WIDGET_STAR_AVE = 999
    const val DEFAULT_VALUE_LONG = -1L
    const val DEFAULT_CARD_ELEVATION_TOOLBAR = 12F
    const val PAGE_SIZE_DEFAULT_LONG = 20L
    const val DEFAULT_VALUE_DOUBLE = -1.0

    const val MODE_SHOULD_UPDATE = 0
    const val MODE_FORCE_UPDATE_KEEP = 1
    const val MODE_FORCE_UPDATE_CLEAR = 2
    const val ACTION_UPDATE_LATE = 3

    const val MODE_NOT_AVAILABLE = 10

    const val STRING_END_LINE = "\n"
    const val STRING_N_A = "N/A"



    const val MODE_ONE = 1
    const val MODE_TWO = 2
    const val MODE_THREE = 3

    const val SHA256_ALGORITHM_ENCRYPT = "SHA-256"
    const val SHA256_SIGNUM = 1
    const val SHA256_RADIX = 16
    const val SHA256_FIXED_LENGTH = 32
    const val KEY_ENCRYPT = "myFSOFT@1234"
    const val KEY_ENCRYPT_EMAIL = "Pe7e4P"
    const val KEY_TEMPLATE_HISTORY = "KEY_TEMPLATE_HISTORY"
    const val STRING_EMPTY = ""
    const val STRING_DOT = "."
    const val STRING_SPACE = " "
    const val STRING_UNDERLINE = "_"
    const val STRING_DASH = "-"
    const val CHAR_EMPTY = ' '
    const val MENTION_PREFIX = '@'
    const val MENTION_START = "@["
    const val MENTION_SEPARATOR = '|'
    const val MENTION_END = ']'
    const val STRING_ROUND_BRACKET_OPEN = "("
    const val CHAR_ROUND_BRACKET_OPEN = '('
    const val CHAR_ROUND_BRACKET_CLOSE = ')'
    const val DEFAULT_WEB_MIME_TYPE = "text/html; charset=utf-8"
    const val WEB_MIME_TYPE_TEXT_HTML = "text/html"
    const val DEFAULT_WEB_ENCODING = "UTF-8"
    const val DEFAULT_LANGUAGE = "vi"
    const val ENGLISH_LANGUAGE = "en"
    const val VIETNAM_LANGUAGE = "vi"
    const val DEFAULT_TIME_TIME_TRACKING_HOUR_FORMAT = "#.##"
    const val KEY_SPLITTER = "splitter"
    const val KEY_UNDER_LINE = "_"

    const val STATUS_REJECTED = "REJECTED"

    const val STATUS_APPROVED = "APPROVED"

    const val HAS_SHOWN_INTRO = "HAS_SHOWN_INTRO"

    const val KEY_BIRTHDAY_UTC = "KEY_BIRTHDAY_UTC"

    const val TIME_SHOW_DIALOG_CMTGD = "2021-02-08T00:00:00Z"
    const val TIME_SHOW_DIALOG_CMTGD_END = "2021-02-17T23:59:59Z"

    const val COMPANY_ID_FHO = "01"

    const val ALPHA_PRESSED = 0.4f
    const val ALPHA_NORMAL = 1f
    const val ALPHA_TRANSPARENT = 0f
    const val ANIM_TIME_SHORT = 100L

    const val ALPHA_ENABLED = 1f
    const val ALPHA_65 = 0.65f
    const val ALPHA_DISABLED = 0.4f
    const val ALPHA_MINIMUM = 0.1f

    const val TIME_VIBRATE = 500L
    const val TIME_DELAY = 50L

    const val TIME_DELAY_50 = 50L

    //Organization chart
    const val ORG_CHART_NAME = "ORG_NAME"
    const val ORG_CHART_NAME_VI = "ORG_NAME_VI"
    const val CONSTANT_ROUND_FLOAT_TO_INT = 0.5f

    //Employee Search
    const val KEY_EMPLOYEE_EMAIL = "KEY_EMPLOYEE_EMAIL"
    const val KEY_EMPLOYEE_SHOW_TOOLBAR = "KEY_EMPLOYEE_SHOW_TOOLBAR"
    const val REQUEST_CODE = 1

    const val KEY_FEATURE_KEY = "KEY_FEATURE_KEY"
    const val KEY_SEARCH_RESULT_DESTINATION = "KEY_SEARCH_RESULT_DESTINATION"
    const val KEY_POST_ITEM = "KEY_POST_ITEM"
    const val KEY_MY_GOLD = "KEY_MY_GOLD"

    const val KEY_TASK = "KEY_TASK"

    //Login
    const val KEY_LOGIN_SUCCEED = "KEY_LOGIN_SUCCEED"
    const val KEY_LOGOUT = "KEY_LOGOUT"


    // credential info... need to encrypt
    const val KEY_ID_TOKEN = "KEY_ID_TOKEN"
    const val KEY_ACCESS_TOKEN = "KEY_ACCESS_TOKEN"
    const val KEY_ADFS_TOKEN = "KEY_ADFS_TOKEN"
    const val KEY_ADFS_REFRESH_TOKEN = "KEY_ADFS_REFRESH_TOKEN"
    const val KEY_SUB = "sub"
    const val KEY_ACCOUNT = "account"
    const val KEY_FULLNAME = "fullname"
    const val KEY_FAMILY_NAME = "familyname"
    const val KEY_PROFILE_IMAGE = "profileimage"
    const val KEY_EMAIL = "email"
    const val KEY_EMAIL_PERSONAL = "emailpersonal"
    const val KEY_POSITION = "position"
    const val KEY_GENDER_TOKEN = "gender"
    const val KEY_DATE_START_WORK = "dateStartWork"
    const val KEY_DATE_OF_BIRTH = "dateOfBirth"
    const val KEY_DEPARTMENT = "departmentName"
    const val KEY_DEPARTMENT_NAME_FULL = "departmentNameFull"
    const val KEY_DEPARTMENT_ID = "departmentId"
    const val KEY_ROLE = "roleIndex"
    const val KEY_IS_MANAGER = "isManager"
    const val KEY_GROUP = "group"
    const val KEY_LIST_FUNCTION = "listFunction"
    const val KEY_SESIONTOKEN = "sessiontokenbookme"
    const val KEY_USER_ID_BOOKING_ROOM = "useridbookme"
    const val KEY_COMPANY_ID = "companyId"
    const val KEY_COMPANY_NAME = "companyName"
    const val KEY_COMPANY_SHORT_DESCRIPTION = "companyShortDescription"
    const val KEY_COMPANY_DESCRIPTION = "companyDescription"
    const val KEY_USER_SERVICE_ID = "userServiceId"
    const val KEY_EMPLOYEE_ID = "employeeId"

    const val KEY_GOOGLE_FITNESS_ACCOUNT = "googlefitnessaccount"

    //guest mode key
    const val KEY_DISPLAY_NAME_GUEST = "fullname"
    const val KEY_EMAIL_GUEST = "email"
    const val KEY_FAMILY_NAME_GUEST = "familyname"
    const val KEY_GIVEN_NAME_GUEST = "givenname"
    const val KEY_TYPE_GUEST = "type"

    const val KEY_AVATAR_ORIGIN = "KEY_AVATAR_ORIGIN"
    const val KEY_AVATAR_MEDIUM = "KEY_AVATAR_MEDIUM"
    const val KEY_AVATAR_SMALL = "KEY_AVATAR_SMALL"

    //Redem
    const val KEY_REDEEM_TYPE = "KEY_REDEEM_TYPE"
    const val KEY_REDEEM_GOLD = "KEY_REDEEM_GOLD"
    const val KEY_FOLLOW_CREATE_PIN_FROM_MY_GOLD = "KEY_FOLLOW_CREATE_PIN_FROM_MY_GOLD"
    const val KEY_REDEEM_GOLD_PIN = "KEY_REDEEM_GOLD_PIN"
    const val KEY_REDEEM_FROM_HOME_FRAGMENT = "KEY_REDEEM_FROM_HOME_FRAGMENT"

    //JiraTask
    const val KEY_PROJECT_NAME = "KEY_PROJECT_NAME"

    //Game
    const val KEY_ITEM_GAME = "KEY_ITEM_GAME"
    const val KEY_TITLE_GAME = "KEY_TITLE_GAME"

    //Bus
    const val KEY_BUS_PERMISSION = "KEY_BUS_PERMISSION"
    const val KEY_CHECK_IN_FROM_HOME_FRAGMENT = "KEY_CHECK_IN_FROM_HOME_FRAGMENT"

    const val MIN_CHAR_TO_SUGGEST_ACCOUNT = 1

    const val SIZE_BUS = "20"
    const val SIZE_SHUTTLE_BUS = 2
    const val SIZE = "10"
    const val PAGE = "1"

    //Declare all type of search here
    enum class SearchType {
        NO_SEARCH, SEARCH_NEWS, SEARCH_ORGANIZATION, SEARCH_BUS,
        SEARCH_EMPLOYEE, SEARCH_NEWS_GUEST, SEARCH_JOB_ALL, SEARCH_BUS_SECRETARY
    }

    // Max line of description in Recognition screen
    const val MAX_LINE = 3
    const val MAX_LINE_TITLE_HISTORY_HEART = 2

    const val CLICK_INTERVAL = 1000L
    const val SECONDS_PER_HOUR = 3600

    const val PHONE_FIXED_LENGTH = 10
    const val KEY_PHONE = "KEY_PHONE"
    const val TIME_ZONE_UTC = "UTC"

    //Quick setting/ (Favorite) item id.
    const val FAV_EMPLOYEE_INFO = 1
    const val FAV_EBUS = 2
    const val FAV_RECOGNITION = 3
    const val FAV_JIRA = 4
    const val FAV_ORG_CHART = 5
    const val FAV_MY_PROFILE = 6
    const val FAV_BUS_INFO = 7
    const val FAV_ERICH = 8
    const val FAV_SERVICE_LIST = 9
    const val FAV_MEETING_ROOM = 10
    const val FAV_ESHAKE = 11
    const val FAV_JOB = 12
    const val FAV_FSOFT_BOOK = 13
    const val FAV_DOMESTIC_BRANCH = 14
    const val FAV_ABOUT_FPT = 15
    const val FAV_TRANING = 16
    const val FAV_MANAGE_TEAM = 17
    const val FAV_FIRST_YEAR = 18
    const val FAV_EVENT = 19
    const val FAV_SURVEY = 20
    const val FAV_LOVE_FPT = 21
    const val FAV_DISCIPLINE = 22
    const val FAV_AI_LAB = 23
    const val FAV_FPT_CARE = 24
    const val FAV_REFER_FRIEND = 25
    const val FAV_HEALTH_DECLARATION = 26
    const val FAV_WFH = 27
    const val FAV_APPROVE_NOW = 28
    const val FAV_ELEARNING = 29
    const val FAV_NEW_JOINER = 30
    const val FAV_REMINDER = 31

    const val KEY_CHECK_SET_QUICK_MENU = "KEY_CHECK_SET_QUICK_MENU"

    const val MAX_QUICK_MENU_ITEM = 4

    const val KEY_COUNTDOWN_CHECK_IN = "KEY_COUNTDOWN_CHECK_IN"
    const val KEY_TIMER_CHECK_IN = "KEY_TIMER_CHECK_IN"

    const val KEY_NOT_SHOW_LOADING = "KEY_NOT_SHOW_LOADING"
    const val KEY_URL_GAME = "KEY_URL_GAME"
    const val KEY_LOAD_KBYT = "NEED_LOAD_KBYT"
    const val INTENT_FILTER_CLOUD_MSG = "firebase-cloud-msg"

    //Notifications
    const val NOTI_CHANNEL_ID = "myFSOFT"
    const val NOTI_CHANNEL_NAME = "myFSOFT channel"
    const val NOTI_CHANNEL_DES = "myFSOFT notification channel"

    const val URL_PLAY_GAME_EMPTY = "https://gamification-mock.letgameit.com/mye/#!/login?token=00052812&source=myfsoft"
    const val URL_DEFAULT_KBYT = "https://ncov.fpt.com.vn"

    const val ACTION_LOCATION = "android.location.PROVIDERS_CHANGED"

    const val FINOS_CORP_CODE = "MYFSOFT"
    const val FINOS_KEY_TOKEN_EXPRIRES_TIME = "FINOS_KEY_TOKEN_EXPRIRES_TIME"

    //Token.. need to be encrypt
    const val FINOS_KEY_TOKEN = "FINOS_KEY_TOKEN"
    const val FINOS_CORP_AM = "FF_FCap"

    //Term.. need to be encrypt
    const val KEY_CHECK_BOX_TERM = "KEY_CHECK_BOX_TERM"
    const val KEY_FULL_NAME = "KEY_FULL_NAME"
    const val KEY_BIRTHDAY = "KEY_BIRTHDAY"
    const val KEY_NATIONAL_ID = "KEY_NATIONAL_ID"
    const val KEY_GENDER = "KEY_GENDER"
    const val KEY_ISSUES_DATE = "KEY_ISSUES_DATE"
    const val KEY_PLACE = "KEY_PLACE"
    const val KEY_TELEPHONE = "KEY_TELEPHONE"
    const val KEY_TPB_NUMBER = "KEY_TPB_NUMBER"


    // feature version
    // key config
    const val PREFIX_ACTIVE = "active"
    const val PREFIX_SHOW = "show"
    const val VERSION_UPDATE = "_version"
    const val STATE_VERSION_CLICK = "version_"
    const val KEY_LIST_WIDGET = "KEY_LIST_WIDGET"
    const val CATEGORY = "category_"

    // key category
    const val KEY_CATEGORY_QUICK_MENU = "quick_menu"
    const val KEY_CATEGORY_GAME = "game"
    const val KEY_CATEGORY_NEWS = "news"
    const val KEY_CATEGORY_UTILITIES = "utilities"
    const val KEY_CATEGORY_WORK = "work"
    const val KEY_CATEGORY_WIKI = "wiki"
    const val KEY_CATEGORY_OTHER_BUTTON = "other"

    // key feature
    const val KEY_EMP_INFO = "wiki_employee"
    const val KEY_SERVICE_LIST = "wiki_servicelist"
    const val KEY_ORG_CHART = "wiki_org"
    const val KEY_BUS_INFO = "wiki_businfo"
    const val KEY_DOMESTIC_BRANCH = "wiki_fsoftbranches"
    const val KEY_FSOFT_BOOK = "wiki_fsoftbook"
    const val KEY_ABOUT_FPT = "wiki_aboutfpt"
    const val KEY_FPT_CARE = "wiki_fptcare"

    const val KEY_RECOGNITION = "work_recognition"
    const val KEY_DISCIPLINE = "work_discipline"
    const val KEY_MEETING_ROOM = "work_meetingroom"
    const val KEY_JOB = "work_postjob"
    const val KEY_WFH = "work_wfh"
    const val KEY_APPROVE_NOW = "work_approvenow"
    const val KEY_LEARNING = "work_learning"
    const val KEY_NEW_JOINER = "work_newjoiner"
    const val KEY_REMINDER = "work_reminder"
    const val KEY_WORK_MANAGE_TEAM = "work_manageteam"
    const val KEY_MOBILE_ACCESS = "smartid_mobileaccess"
    const val KEY_GIFT_BE_STRONG = "be_strong_gift_to_dn"

    const val KEY_EBUS = "smartid_ebus"
    const val KEY_ESHAKE = "smartid_eshake"
    const val KEY_EVENTS = "smartid_event"
    const val KEY_SURVEYS = "smartid_survey"
    const val KEY_AI_LAB = "smartid_ailab"
    const val KEY_LOVE_FPT = "smartid_lovefpt"
    const val KEY_REFER_FRIEND = "smartid_refer_friend"
    const val KEY_POST_JOB_APPLY = "work_postjob_apply"
    const val KEY_TAB_BE_STRONG = "tab_be_strong"
    const val KEY_TAB_ANNOUNCEMENT = "tab_announcement"
    const val KEY_TAB_POST_JOB = "tab_post_job"
    const val KEY_POST_JOB_RUF = "work_postjob_ruf"
    const val KEY_WORK_TRAINING = "work_training"

    const val KEY_HEALTH_DECLARATION = "smartid_health_declaration"

    const val KEY_NEWS = "tab_news"
    const val KEY_STAR_AVE = "tab_star_ave"

    const val KEY_GAME_LIST = "game_gamelist"

    const val KEY_MY_PROFILE = "setting_myprofile"

    const val KEY_REDEEM_CASH = "redeem_cash"
    const val KEY_REDEEM_UTOP = "redeem_utop"
    const val KEY_REDEEM_GIFT = "redeem_gift"
    const val KEY_SETTING_AVATAR = "setting_avatar"
    const val KEY_JOB_APPLY = "postjob_apply"
    const val KEY_JOB_RUF = "postjob_ruf"
    const val KEY_PAYSLIP = "my_profile_layslip"

    const val SHOULD_UPDATE_DIALOG_OPENED = "should_update_dialog_opened"
    const val SHOULD_UPDATE_DIALOG_OPEN = "should_update_dialog_open"
    const val LOGOUT_BY_FORCE_UPDATE_MESSAGE = "logout_by_force_update_message"

    const val DEFAULT_GOLD_REDEEM_CASH = 100
    const val REGEX_REDEEM = "[^\\d]"

    const val RECTANGLE = "rectangle"
    const val OVAL = "oval"

    const val RECTANGLE_ = "Rectangle"
    const val OVAL_ = "Oval"

    const val KEY_CHECK_SHOW_DIALOG_OOPS_REDEEM = "KEY_CHECK_SHOW_DIALOG_OOPS_REDEEM"

    // Star ave
    const val KEY_STAR_AVE_RECOGNIZE = "KEY_STAR_AVE_RECOGNIZE"

    // All apps
    const val KEY_ALL_APP_GRID_SATE = "KEY_ALL_APP_GRID_SATE"

    //Meeting Room
    const val KEY_CHECK_CONFIRM_BOOKING_ROOM = "KEY_CHECK_CONFIRM_BOOKING_ROOM"
    const val KEY_CHECK_DATA_BOOKING_ROOM = "KEY_CHECK_DATA_BOOKING_ROOM"
    const val KEY_MEETING_ROOM_DETAIL = "KEY_MEETING_ROOM_DETAIL"
    const val STATUS_CREATE = "Created"
    const val STATUS_PENDING = "Pending"

    const val SEARCH_FIND_FREE_TYPE = 1
    const val SEARCH_VIEW_STATUS = 2
    const val KEY_DATA_RESERVATION_OBJECT = "KEY_DATA_RESERVATION_OBJECT"
    const val BOOKED_TIME = 1
    const val CLICKED_TIME = 2
    const val UNCLICKED_TIME = 0
    const val MAX_LENGTH_SEAT = 3
    const val ACTION_INVOKE_LOG_OUT = 0

    //Qms
    const val KEY_QMS_CATEGORY_OBJECT = "KEY_QMS_CATEGORY_OBJECT"
    const val KEY_QMS_PDF = "KEY_QMS_PDF"

    // Surveys
    const val KEY_SURVEYS_ITEM = "KEY_SURVEYS_ITEM"
    const val KEY_SURVEYS_ACTION = "KEY_SURVEYS_ACTION"
    const val KEY_SURVEYS_ID = "KEY_SURVEYS_ID"

    //Event
    const val FIELD_DISTANCE = "distance"
    const val FIELD_STEPS = "steps"
    const val SCHEMA_PACKAGE = "package"

    const val KEY_IMAGE_HEADER_INTRO_EVENT = "KEY_IMAGE_HEADER_INTRO_EVENT"
    const val KEY_CONTENT_INTRO_EVENT = "KEY_CONTENT_INTRO_EVENT"

    const val KEY_IMAGE_HEADER_INTRO_FTS = "KEY_IMAGE_HEADER_INTRO_FTS"
    const val KEY_CONTENT_INTRO_FTS = "KEY_CONTENT_INTRO_FTS"
    const val KEY_CONTENT_FTS = "KEY_CONTENT_FTS"
    const val KEY_CONTENT_DETAIL_EPAYMENT = "KEY_CONTENT_DETAIL_EPAYMENT"
    const val KEY_STATUS_EPAYMENT ="KEY_STATUS_EPAYMENT"
    const val KEY_EPAYMENT ="KEY_EPAYMENT"
    const val KEY_COMMENT_EPAYMENT="KEY_COMMENT_EPAYMENT"

    const val KEY_TOP_RANK_EVENT = "KEY_TOP_RANK_EVENT"

    const val KEY_FOLLOW_THE_SUN_EVENT = "KEY_FOLLOW_THE_SUN_EVENT"
    const val KEY_SEARCH_USER_FLTS = "KEY_SEARCH_USER_FLTS"
    const val KEY_FOLLOW_THE_SUN_EVENT_ID = "KEY_FOLLOW_THE_SUN_EVENT_ID"
    const val KEY_FOLLOW_THE_SUN_STEP_USER = "KEY_FOLLOW_THE_SUN_STEP_USER"
    const val KEY_FOLLOW_THE_SUN_LIST_RANKING = "KEY_FOLLOW_THE_SUN_LIST_RANKING"
    const val KEY_FTS_GROUP_DETAIL = "KEY_FTS_GROUP_DETAIL"

    const val KEY_BUNDLE_POST_ITEM = "KEY_BUNDLE_POST_ITEM"

    //E-Shake
    const val KEY_CHECK_SEND_DATA_FROM = "KEY_CHECK_SEND_DATA_FROM"
    const val KEY_CHECK_SEND_DATA_FROM_HISTORY = "KEY_CHECK_SEND_DATA_FROM_HISTORY"
    const val KEY_SHAKING_TO_SHAKE_FORM = "KEY_SHAKING_TO_SHAKE_FORM"
    const val KEY_SHAKE_NEAR_BY_TO_SHAKING = "KEY_SHAKE_NEAR_BY_TO_SHAKING"
    const val KEY_SHAKE_AUTO_FINISH = "KEY_SHAKE_AUTO_FINISH"
    const val KEY_SWITCH_JOB_APPLIED_TAB = "KEY_SWITCH_JOB_APPLIED_TAB"
    const val KEY_SWITCH_JOB_RUF_TAB = "KEY_SWITCH_JOB_RUF_TAB"
    const val KEY_SHAKE_CHECK_ICON = "KEY_SHAKE_CHECK_ICON"
    const val KEY_SHAKE_CHECK_SHAKE_WITH = "KEY_SHAKE_CHECK_SHAKE_WITH"
    const val KEY_SHAKE_CHECK_DATE = "KEY_SHAKE_CHECK_DATE"
    const val KEY_SHAKE_CHECK_TIME = "KEY_SHAKE_CHECK_TIME"
    const val KEY_SHAKE_CHECK_NOTE = "KEY_SHAKE_CHECK_NOTE"
    const val KEY_SHAKE_CHECK_ID = "KEY_SHAKE_CHECK_ID"
    const val KEY_SHAKE_CHECK_ANSWER_ID = "KEY_SHAKE_CHECK_ANSWER_ID"
    const val KEY_SHAKE_CHECK_ANSWER_CONTENT = "KEY_SHAKE_CHECK_ANSWER_CONTENT"
    const val KEY_SHAKE_CHECK_QUESTION_CONTENT = "KEY_SHAKE_CHECK_QUESTION_CONTENT"
    const val KEY_SHAKE_CHECK_SHAKE_WITH_SHAKING = "KEY_SHAKE_CHECK_SHAKE_WITH_SHAKING"
    const val KEY_SHAKE_CHECK_DATE_SHAKING = "KEY_SHAKE_CHECK_DATE_SHAKING"
    const val KEY_SHAKE_CHECK_TIME_SHAKING = "KEY_SHAKE_CHECK_TIME_SHAKING"
    const val KEY_SHAKE_CHECK_NOTE_SHAKING = "KEY_SHAKE_CHECK_NOTE_SHAKING"
    const val KEY_SHAKE_CHECK_ID_SHAKING = "KEY_SHAKE_CHECK_ID_SHAKING"
    const val KEY_SHAKE_CHECK_ID_ANSWER_SHAKING = "KEY_SHAKE_CHECK_ID_ANSWER_SHAKING"
    const val KEY_CHECK_UPDATE_DATA = "KEY_CHECK_UPDATE_DATA"
    const val KEY_SHAKE_SEND_ID_PENDING = "KEY_SHAKE_SEND_ID_PENDING"
    const val KEY_SHAKE_IS_PENDING = "KEY_SHAKE_IS_PENDING"
    const val KEY_CANCEL_PENDING = "KEY_CANCEL_PENDING"
    const val KEY_SWIPE_TO_TAB_CANCEL_PENDING = "KEY_SWIPE_TO_TAB_CANCEL_PENDING"
    const val KEY_SHAKING_FLOATING_BUTTON = "KEY_SHAKING_FLOATING_BUTTON"
    const val KEY_HEIGHT_VIEW_PENDING = "KEY_HEIGHT_VIEW_PENDING"
    const val ALL_BU = "ALL BU"

    const val KEY_SHAKE_SEND_DATA_MY_STAFF = "KEY_SHAKE_SEND_DATA_MY_STAFF"

    const val TYPE_ALL = "all"
    const val TYPE_NA = "na"
    const val TYPE_BAD = "bad"
    const val TYPE_NORMAL = "normal"
    const val TYPE_GOOD = "good"
    const val ID_DELETE_PENDING = 1

    // Key My Achievement
    // all value is in lowercase, so let compare ignore case
    const val TYPE_RECOGNITION = "recognition"
    const val TYPE_RECOGNITION_GROUP = "recognition_group"

    const val TYPE_CAMPAIGN = "campaign"
    const val TYPE_GAME = "game"
    const val TYPE_LUCKINESS = "luckiness"

    const val TYPE_GIFT = "gift"
    const val TYPE_ECOUPON = "ecoupon"
    const val TYPE_EBUS = "ebus"

    const val TYPE_UTOP = "utop"
    const val TYPE_CASH = "cash"

    const val TYPE_WITHDRAW = "withdraw"

    const val UTOP_STATUS_DONE = "done"
    const val UTOP_STATUS_UNDONE = "undone"
    const val UTOP_STATUS_FAILED = "failed"
    const val UTOP_STATUS_RECOVER_MANUALLY = "recover manually"

    const val KEY_LOGIN_CASE = "KEY_LOGIN_CASE"
    const val KEY_GUEST_MODE = "KEY_GUEST_MODE"
    const val KEY_FACEBOOK = "facebook"
    const val KEY_GOOGLE = "google"

    const val KEY_POST_JOB = "KEY_POST_JOB"
    const val KEY_POST_JOB_DATA = "KEY_POST_JOB_DATA"
    const val KEY_POST_STAR_AVE = "KEY_POST_STAR_AVE"
    const val KEY_ID_STAR_AVE = "KEY_ID_STAR_AVE"
    const val KEY_RECOGNIZE_ID_STAR_AVE = "KEY_RECOGNIZE_ID_STAR_AVE"
    const val STAR_AVE_RECOGNIZE_TYPE_INDIVIDUAL = "individual"

    const val ALPHA_JOB = 20
    const val ID_JOB_DEVELOPER = "Developer"
    const val ID_JOB_DESIGNER = "Designer"
    const val ID_JOB_BUSINESSANALYST = "BusinessAnalyst"
    const val ID_JOB_BUSINESS_ANALYST = "Business Analyst"
    const val ID_JOB_COMTOR = "Comtor"
    const val ID_JOB_OTHER = "Other"
    const val ID_JOB_OTHERS = "Others"
    const val ID_JOB_QUALITYASSURANCE = "QualityAssurance"
    const val ID_JOB_QUALITY_ASSURANCE = "Quality Assurance"
    const val ID_JOB_SOLUTIONARCHITECT = "SolutionArchitect"
    const val ID_JOB_SOLUTION_ARCHITECT = "Solution Architect"
    const val ID_JOB_TESTER = "Tester"

    const val SUBMIT_TYPE_APPLY = 1
    const val SUBMIT_TYPE_RUF = 2
    const val SUBMIT_TYPE_REPORT_ISSUE = 1
    const val ALPHA_STATUS = 0
    const val ID_STATUS_APPLIED = "Applied"
    const val ID_STATUS_INTERVIEWED = "interviewed"
    const val ID_STATUS_ONBOARDING = "onboarding"
    const val ID_STATUS_QUIT = "quit"
    const val ID_STATUS_REFUSE = "refuse"
    const val ID_STATUS_OFFERING = "offer"
    const val ID_STATUS_HIRED = "hired"
    const val ID_STATUS_ARCHIVED = "archived"

    const val DESC = "desc"
    const val ASC = "asc"

    const val KEY_UUID_SEVER = "KEY_UUID_SEVER"
    const val KEY_REGISTER_EBUS = "KEY_REGISTER_EBUS"
    const val KEY_EBUS_LOCATION = "KEY_EBUS_LOCATION"
    const val KEY_EBUS_LOCATION_SEARCH = "KEY_EBUS_LOCATION_SEARCH"
    const val KEY_EBUS_LOCATION_CHECK_IN = "KEY_EBUS_LOCATION_CHECK_IN"
    const val KEY_EBUS_MY_ROUTE = "KEY_EBUS_MY_ROUTE"
    const val KEY_EBUS_QR = "KEY_EBUS_QR"

    const val KEY_LAST_TIME_UPDATE_AVATAR = "KEY_LAST_TIME_UPDATE_AVATAR"

    const val MEDAL_GREAT_JOB = 1
    const val MEDAL_AMAZING_SPIRIT = 2
    const val MEDAL_TOP_PERFORMER = 3
    const val MEDAL_GREAT_LEADER = 4
    const val MEDAL_BEST_SUPPORT = 5
    const val MEDAL_EXCELLENT_OKR = 6

    //Sync Favorite
    const val MODE_SYNC_NONE_FAVORITE = 0
    const val MODE_SYNC_ADD_FAVORITE = 1
    const val MODE_SYNC_FAVORITE_FROM_DETAIL = 2
    const val MODE_SYNC_FAVORITE_POST_JOB = 3
    const val MODE_CLICK_FAVORITE = 4

    //Sync Apply and Ruf
    const val MODE_SYNC_NONE = 0
    const val MODE_CHECK_CALL_API = 1

    const val MODE_CHECK_NONE_REFRESH = 0
    const val MODE_CHECK_REFRESH = 1

    const val TYPE_FAVORITE = "TYPE_FAVORITE"
    const val TYPE_APPLY = "TYPE_APPLY"

    const val TYPE_CHANGE_TAB = 0
    const val TYPE_REFRESH = 1

    const val TYPE_SYNC = "TYPE_SYNC"
    const val TYPE_NO_SYNC = "TYPE_NO_SYNC"

    const val KEY_GOLD_IN = "KEY_GOLD_IN"
    const val KEY_GOLD_OUT = "KEY_GOLD_OUT"

    const val KEY_POSITION_TAB_NEWS = "KEY_POSITION_TAB_NEWS"

    const val COMPANY_ID_FSOFT = "66"
    const val KEY_DATA_GAME_LUCKY_MONEY = "KEY_DATA_GAME_LUCKY_MONEY"
    const val KEY_DATA_GAME_LUCKY_NUMBER = "KEY_DATA_GAME_LUCKY_NUMBER"
    const val KEY_DATA_GAME_LUCKY_ID = "KEY_DATA_GAME_LUCKY_ID"
    const val KEY_DATA_GAME_LUCKY_TITLE = "KEY_DATA_GAME_LUCKY_TITLE"
    const val KEY_DATA_MONEY_PREFIX = "KEY_DATA_MONEY_"

    const val KEY_CODE_REDEEM_CENT = "KEY_CODE_REDEEM_CENT"
    const val KEY_CODE_REDEEM_CENT_CONFIRM = "KEY_CODE_REDEEM_CENT_CONFIRM"

    const val KEY_TAB_INDEX = "KEY_TAB_INDEX"

    const val KEY_BUDGET_POINT = "KEY_BUDGET_POINT"
    const val KEY_DATA_BUDGET_GOLD = "KEY_DATA_BUDGET_GOLD"
    const val KEY_MODE_PIN = "KEY_MODE_PIN"
    const val KEY_MODE_PIN_TITLE = "KEY_MODE_PIN_TITLE"
    const val KEY_MESS_HAPPY_NEW_YEAR = "KEY_MESS_HAPPY_NEW_YEAR"

    const val KEY_EBUS_OR_EVENT = "KEY_EBUS_OR_EVENT"
    const val KEY_EVENT_FORM_URL = "KEY_EVENT_FORM_URL"
    const val KEY_EVENT_FORM_TYPE = "KEY_EVENT_FORM_TYPE"
    const val KEY_EVENT_FORM_NAME = "KEY_EVENT_FORM_NAME"

    const val KEY_EVENT_DATA = "KEY_EVENT_DATA"
    const val KEY_EVENT_ID = "EVENT_ID"
    const val KEY_CHECKIN_OR_CHECKOUT = "KEY_CHECKIN_CHECKOUT"

    const val KEY_SURVEY_HISTORY = "KEY_SURVEY_HISTORY"
    const val KEY_SURVEY_BASE_LINK = "KEY_SURVEY_BASE_LINK"

    const val DISCIPLINE_SHOW_INTERNAL = "DISCIPLINE_SHOW_INTERNAL"
    const val DISCIPLINE_SHOW_GROUP = "DISCIPLINE_SHOW_GROUP"
    const val DISCIPLINE_ROLE = "DISCIPLINE_ROLE"
    const val DISCIPLINE_INTERNAL = 0
    const val DISCIPLINE_INTERNAL_WITH_INCOME_CUT = 1
    const val DISCIPLINE_CORPORATION = 2

    const val KEY_SEARCH_USER_APPROVE_NOW = "KEY_SEARCH_USER_APPROVE_NOW"
    const val KEY_SEARCH_USER = "KEY_SEARCH_USER"
    const val KEY_SEARCH_MYSELF = "KEY_SEARCH_MYSELF"
    const val KEY_SEARCH_INTERNAL = "KEY_SEARCH_INTERNAL"
    const val KEY_CONTENT_VIEW_ID = "KEY_CONTENT_VIEW_ID"
    const val KEY_HIDE_TOOLBAR = "KEY_HIDE_TOOLBAR"
    const val KEY_SEARCH_STATUSBAR_FULL = "KEY_SEARCH_STATUSBAR_FULL"

    const val DISCIPLINE_RESPONSE_PREVIEW = "PREVIEW_RESPONSE"
    const val DISCIPLINE_RESPONSE_TEMPLATE = "TEMPLATE_RESPONSE"
    const val DISCIPLINE__TYPE_TEMPLATE = "DISCIPLINE__TYPE_TEMPLATE"
    const val DISCIPLINE_TYPE_VIEW_DETAILS = "DISCIPLINE_TYPE_VIEW_DETAILS"
    const val DISCIPLINE_RESPONSE_VIEW_DETAILS = "DISCIPLINE_RESPONSE_VIEW_DETAILS"

    const val LOVE_FPT_STATUS_SUBMITTED = "Submitted"
    const val LOVE_FPT_STATUS_PROCESSING = "Processing"
    const val LOVE_FPT_STATUS_COMPLETE = "Completed"
    const val LOVE_FPT_STATUS_CANCEL = "Cancelled"
    const val KEY_LOVE_FPT_FOR_RULE = "KEY_LOVE_FPT_FOR_RULE"

    const val LOVE_FPT_TYPE_EMPLOYEE = "Employee"
    const val LOVE_FPT_TYPE_FRIEND = "Friend"
    const val KEY_LOVE_FPT_FILTER_CATEGORY = "KEY_LOVE_FPT_FILTER_CATEGORY"
    const val KEY_LOVE_FPT_SENIORITY = "KEY_LOVE_FPT_SENIORITY"
    const val KEY_LOVE_FPT_TOTAL_SPENT_MONEY = "KEY_LOVE_FPT_TOTAL_SPENT_MONEY"
    const val KEY_LOVE_FPT_TYPE_BUY = "KEY_LOVE_FPT_TYPE_BUY"
    const val KEY_LOVE_CONFIRM_SHARE_INFO = "KEY_LOVE_CONFIRM_SHARE_INFO"
    const val KEY_LOVE_ALLOW_FRIEND_BUY = "KEY_LOVE_ALLOW_FRIEND_BUY"

    const val KEY_DATA_ORDER_DETAILS_OBJECT = "Key data order detail object"

    //LoveFPT Order Details Cancel Order
    const val CANCEL_ORDER_SUCCESS_200 = "200"
    const val CANCEL_ORDER_SUCCESS_201 = "201"

    const val FPT_BOOK_DOCUMENT_CATEGORY_ID = "FPT_BOOK_DOCUMENT_CATEGORY_ID"
    const val FPT_BOOK_DOCUMENT_CATEGORY_TYPE = "FPT_BOOK_DOCUMENT_CATEGORY_TYPE"
    const val FPT_BOOK_DOCUMENT_ITEM = "FPT_BOOK_DOCUMENT_ITEM"
    const val FPT_BOOK_DOCUMENT_CATEGORY_IS_SHOW = "FPT_BOOK_DOCUMENT_ITEM_BOOLEAN"

    //Fsoft book
    const val LOW_LEVEL = 1
    const val MEDIUM_LEVEL = 2
    const val HIGH_LEVEL = 3
    const val SHOW_CATEGORY_0 = 0

    const val MODE_GRID = 1
    const val MODE_LIST = 2

    const val FPT_BOOK_DOCUMENT_CATEGORY = "FPT_BOOK_DOCUMENT_CATEGORY"
    const val FPT_BOOK_DOCUMENT_SHORT_ITEM = "FPT_BOOK_DOCUMENT_SHORT_ITEM"
    const val FPT_BOOK_DOCUMENT_ID = "FPT_BOOK_DOCUMENT_ID"
    const val TYPE_CATEGORY = "Category"
    const val TYPE_LANGUAGE = "Language"
    const val TYPE_POSITION = "Position"
    const val FPT_BOOK_ISFILTER = "ISFILTER"
    const val FPT_BOOK_TYPE_PREVIEW = "FPT_BOOK_TYPE_PREVIEW"
    const val FPT_BOOK_TYPE_PREVIEW_FROM_LIST = "FPT_BOOK_TYPE_PREVIEW_FROM_LIST"
    const val FPT_BOOK_TYPE_PREVIEW_FROM_HOME = "FPT_BOOK_TYPE_PREVIEW_FROM_HOME"
    const val FPT_BOOK_DATA_ROLE_ALL = 0
    const val FPT_BOOK_DATA_ROLE_FSOFTER = 1
    const val FPT_BOOK_DATA_MANAGER = 2
    const val FPT_BOOK_ROLE_ID = "FPT_BOOK_ROLE_ID"

    // FPT Care
    const val KEY_HDTT_DETAIL_FPT_CARE = "KEY_HDTT_DETAIL_FPT_CARE"
    const val KEY_HDTT_DETAIL_TITLE_FPT_CARE = "KEY_HDTT_DETAIL_TITLE_FPT_CARE"
    const val KEY_SEARCH_DSCSYT = "KEY_SEARCH_DSCSYT"
    const val TYPE_SEARCH_DSCSYT = 0
    const val TYPE_SEARCH_DSCSYTLT = 1
    const val PREFIX_SERVICE_HOSPITAL = ","

    const val TYPE_INTERNAL = "internal"
    const val TYPE_CORPORATION = "corporation"
    const val KEY_FPT_CARE_LIST_COMPANY = "KEY_FPT_CARE_LIST_COMPANY"

    const val KEY_FPT_CARE_HISTORY_SETTLEMENT = "KEY_FPT_CARE_HISTORY_SETTLEMENT"
    const val STRING_ALL_FPT_CARE = "ALL"
    const val STRING_ALL_FPT_CARE_VI = "Tất cả"
    const val STRING_T = "T."
    const val STRING_T_DOT = "T"
    const val STRING_DAY = "Thứ "
    const val STRING_TH = "Th"
    const val STRING_CN = "CN"
    const val STRING_THG = "thg "
    const val KEY_QLHB_POSITION_TAB_LEVEL = "KEY_QLHB_POSITION_TAB_LEVEL"
    const val KEY_QLBH_FPT_CARE = "KEY_QLBH_FPT_CARE"
    const val KEY_USER_LEVEL_FPT_CARE = "KEY_USER_LEVEL_FPT_CARE"

    const val KEY_SUCCESS_EBUS = "KEY_SUCCESS_EBUS"
    const val KEY_TIME_SUCCESS_EBUS = "KEY_TIME_SUCCESS_EBUS"

    const val TYPE_SUCCESS = 0L
    const val TYPE_SUCCESS_WITH_DIFFERENT_ROUTE = 1L
    const val TYPE_SUCCESS_WITH_NOT_REGISTER = 2L
    const val TYPE_ONLINE = 0L
    const val TYPE_OFFLINE = 1L

    const val KEY_UUID_DEFAULT = "B9407F30-F5F8-466E-AFF9-25556B57FE6D"

    // Integration FTEL
    const val STYLE_SHARE = "text/plain"
    const val KEY_CONTRACT_INTEGRATION = "KEY_CONTRACT_INTEGRATION"
    const val KEY_OPEN_PROMOTION_RULE_FROM_CONTRACT_DETAIL = "KEY_OPEN_PROMOTION_RULE_FROM_CONTRACT_DETAIL"
    const val KEY_POLICY = "KEY_POLICY"
    const val KEY_CODE = "KEY_CODE"
    const val KEY_ID_REFER_FRIEND = "KEY_ID_REFER_FRIEND"

    const val STATE_VIEW_KEY = "STATE_VIEW_KEY"

    //Be Strong DN
    const val GOLD_THUONG_THUONG = "Thương thương"
    const val GOLD_15 = "15"
    const val GOLD_30 = "30"
    const val GOLD_45 = "45"
    const val GOLD_OTHER = "Other"
    const val KEY_GET_CURRENT_GOLD = "GET_CURRENT_GOLD"
    const val ACTION_SEE_MORE = 0

    const val ANIMATION_PIVOT = 0.5f

    const val LOCATION_HN_ID = 0
    const val LOCATION_DN_ID = 1
    const val LOCATION_HCM_ID = 2

    const val LOCATION_HN_CODE = "HN"
    const val LOCATION_DN_CODE = "DN"
    const val LOCATION_HCM_CODE = "HCM"

    const val HOTLINE_HN = "1900633926"
    const val PHONE_DEFAULT_SECRETARY = "0944855818"
    const val PHONE_DEFAULT_SECRETARY_DN = "0978406146"
    const val PHONE_DEFAULT_SECRETARY_HCM = "0982302738"
    const val KEY_BACK_TO_HOME_FROM_DIA_LOG_BUS_HOME = "-1"

    const val KEY_LINK_REGISTER_BUS = "KEY_LINK_REGISTER"

    const val DURATION_ANIMATION_ARROW = 100L
    const val DEFAULT_ROTATION_ARROW = 0f
    const val EXPAND_ROTATION_ARROW = 90f
    const val REVERSE_ROTATION_ARROW = 180f

    const val APPROVED = "APPROVED"
    const val REJECTED = "REJECTED"


    // Approve Now - U-Service
    const val KEY_TICKET_ITEM = "KEY_TICKET_ITEM"
    const val KEY_TICKET_DETAIL_WORKFLOW = "KEY_TICKET_DETAIL_WORKFLOW"
    const val KEY_APPROVE_NOW_PHASE_OUTPUT_ID = "KEY_APPROVE_NOW_PHASE_OUTPUT_ID"
    const val KEY_APPROVE_NOW_DETAIL = "KEY_APPROVE_NOW_DETAIL"
    const val KEY_TENANT_ID = "KEY_TENANT_ID"
    const val KEY_APPROVE_NOW_TYPE_USERVICE = "approvenow_uservice"
    const val KEY_APPROVE_NOW_TYPE_EPAYMENT = "approvenow_epayment"
    const val KEY_APPROVE_NOW_TYPE_EPURCHASE = "approvenow_epurchase"
    const val KEY_APPROVE_NOW_TYPE_TMS = "approvenow_tms"

    const val KEY_POSITION_TAB_LAYOUT_SEARCH = "KEY_POSITION_TAB_LAYOUT_SEARCH"
    const val VALUE_SPLIT_PATH = "/"
    const val INDEX_NOT_FOUND = -1

    const val KEY_TICKET_ID = "KEY_TICKET_ID"
    const val KEY_REGISTED_USERVICE = "KEY_REGISTED_USERVICE"
    const val KEY_VIEW_FILE_PDF_APPROVE_NOW = "KEY_VIEW_FILE_PDF_APPROVE_NOW"
    const val KEY_SHOW_FOOTER_APPROVE = "KEY_SHOW_FOOTER_APPROVE"
    const val KEY_DETAIL_TICKET_RESPONSE_APPROVE_NOW = "KEY_DETAIL_TICKET_RESPONSE_APPROVE_NOW"
    const val KEY_LIST_OUT_PUT_INFO_APPROVE_NOW = "KEY_LIST_OUT_PUT_INFO_APPROVE_NOW"
    const val KEY_ITEM_COURE_CHECK_IN = "KEY_ITEM_COURE_CHECK_IN"
    const val KEY_PHASE_OWNER = "Sender"
    const val KEY_SUBMITTED_TIME = "Submitted Time"

//    CMSN FPT
    const val KEY_SHOW_POPUP_CMSN_IN_DAY = "KEY_SHOW_POPUP_CMSN_IN_DAY"
    const val KEY_VERSION = "KEY_VERSION"
    //    CMSN Employee
    const val KEY_HAS_SHOWN_POPUP_HPBD_EMPLOYEE = "KEY_SHOW_POPUP_HPBD_EMPLOYEE"

    const val TYPE_PRODUCT_ORDER = "WEBVIEW"
    const val TYPE_STATUS_SUBMITTED = "Submitted"
    //Learning
    const val KEY_LEARNING_CHECK = "KEY_LEARNING_CHECK"
    const val KEY_OK = "OK"
    const val KEY_CANCEL = "Cancel"

    const val NAME_OUTLOOK = "outlook"
    const val NAME_END_MAIL = ".gm"
    const val NAME_GMAIL = "gmail"

    const val KEY_PUT_DATA_ADMIN = "KEY_PUT_DATA_ADMIN"
    const val KEY_CHECK_ADMIN_SETTING = "KEY_CHECK_ADMIN_SETTING"
    const val OLD_VALUE_MONTH_FTS_ADMIN = "Thg"
    const val NEW_VALUE_MONTH_FTS_ADMIN = "Th"
    const val KEY_LIST_NOT_SEARCH = "KEY_LIST_NOT_SEARCH"

    const val TYPE_LINK_HTTP = "http://"
    const val TYPE_LINK_HTTPS = "https://"
    const val KEY_LANGUAGE = "KEY_LANGUAGE"

    const val KEY_OPEN_LINK_NOTI_CUSTOM = "KEY_OPEN_LINK_NOTI_CUSTOM"
    const val KEY_OPEN_NOTI_FRAGMENT = "KEY_OPEN_NOTI_FRAGMENT"

    const val KEY_LOCATION_CONTACTS = "KEY_LOCATION_CONTACTS"

    const val KEY_STRING_NAME = "string"

    const val PATTERN_FORMAT_STRING_TYPE = "%s %s"
    const val ONE_SECOND = 60
    const val QUALITY_IMAGE = 100

    const val KEY_IS_PUSHING_NOTI_EBUS = "KEY_IS_PUSHING_NOTI_EBUS"
    const val KEY_SAVE_CHECK_DAY = "KEY_SAVE_CHECK_DAY"

    const val KEY_ITEM_RECOMMEND_ID = "KEY_ITEM_RECOMMEND_ID"
    const val KEY_COUNT_WORK_OF_MONTH = "KEY_COUNT_WORK_OF_MONTH"
    const val KEY_SAVE_STATE_LIKE_OR_DISLIKE = "KEY_SAVE_STATE_LIKE_OR_DISLIKE"
    const val KEY_SHOW_INTRODUCTION_NEW_JOINER = "KEY_SHOW_INTRODUCTION_NEW_JOINER"
    const val KEY_TIME_DAILY_NEW_JOINER = "KEY_TIME_DAILY_NEW_JOINER"
    const val KEY_SHOW_DAILY_NEW_JOINER = "KEY_SHOW_DAILY_NEW_JOINER"
    const val KEY_NEW_JOINER_TO_ESHAKE = "KEY_NEW_JOINER_TO_ESHAKE"
    const val KEY_NEW_JOINER_TO_RECOGNIZE = "KEY_NEW_JOINER_TO_RECOGNIZE"
    const val KEY_NEW_JOINER_TO_QUIZ = "KEY_NEW_JOINER_TO_QUIZ"
    const val KEY_NEW_JOINER_DAY_WORK = "KEY_NEW_JOINER_DAY_WORK"
    const val KEY_NEW_JOINER_FROM_HOME = "KEY_NEW_JOINER_TO_HOME"
    const val KEY_NEW_JOINER_VALUE_POINT_RECOGNIZE = "KEY_NEW_JOINER_VALUE_POINT_RECOGNIZE"
    const val KEY_URL_HEADER_HOME_IMG = "KEY_URL_HEADER_HOME_IMG"
    const val KEY_IMAGE_HEADER = "KEY_IMAGE_HEADER"
    const val KEY_SHOW_PAMS = "KEY_SHOW_PAMS"
    const val KEY_TICKET_DNMS = "KEY_TICKET_DNMS"
    const val KEY_FROM_HISTORY = "KEY_FROM_HISTORY"
    const val KEY_VALUE_DATE_APPROVAL = "KEY_VALUE_DATE_APPROVAL"
    const val KEY_VALUE_STATE_APPROVAL = "KEY_VALUE_STATE_APPROVAL"
    const val KEY_CURRENT_TAB = "KEY_CURRENT_TAB"

    const val URL_CHROME = "com.android.chrome"
    const val URL_TEAMS = "https://teams.microsoft.com/l/chat/0/0?users="

    // screen size
    const val SCREEN_SIZE_LOW = "LOW"
    const val SCREEN_SIZE_MEDIUM = "MEDIUM"
    const val SCREEN_SIZE_HIGH = "HIGH"
    const val SCREEN_SIZE_XHIGH = "XHIGH"
    const val SCREEN_SIZE_XXHIGH = "XXHIGH"
    const val SCREEN_SIZE_XXXHIGH = "XXXHIGH"

    const val KEY_RELOGIN_FOR_SUBFUNCTION = "KEY_RELOGIN_FOR_SUBFUNCTION"
    const val KEY_RELOGIN_SAME_USER = "KEY_RELOGIN_SAME_USER"

    const val YEARLY_PAYSLIP = "YEARLY_PAYSLIP"
    const val DETAIL_DATA_MONTHLY_PAYSLIP = "DETAIL_DATA_MONTHLY_PAYSLIP"
    const val FORMAT_NUMBER_ROUNDING = "%.0f"

    const val KEY_LUCKY_WORD_HL = "KEY_LUCKY_WORD_HL"

    // epayment
    const val ADVANCEPAYMENT = "ADVANCEPAYMENT"
    const val PREPAYMENT ="PREPAYMENT"
    const val PAYMENT ="PAYMENT"

}
